import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    touchableButton: {
        flex: 1,
        justifyContent: 'center',
        alignContent: 'center',
        padding: 10
    },

    expressionText: {
        color: '#FF5722',
        fontSize: 38,
        textAlign: 'right'
    },

    resultText: {
        color: '#FF5722',
        fontSize: 24,
        textAlign: 'right'
    },

    buttonTextNumber: {
        color: '#FFFFFF',
        fontSize: 24,
        textAlign: 'center'
    },

    buttonTextOperand:{
        color: '#FF5722',
        fontSize: 24,
        textAlign: 'center'
    }
})