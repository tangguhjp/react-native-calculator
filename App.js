import React, { Component } from 'react';
import { View, StatusBar, Text, TouchableOpacity} from 'react-native';
import { terbilang } from './src/terbilang';
import styles from './src/styles';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      result: '',
      resultNumber: '',
      calculate: '0',
     };
  }
  
  isOperand = (value) => {
    if(['/', '*', '+', '-'].includes(value)){
      return true
    }
    return false
  }

  inputNumber = (value) => {
    var calculate = this.state.calculate
    if(this.state.calculate == '0'){
      if(value == '.'){
        this.setState({calculate: (calculate.includes('.') ? this.state.calculate : this.state.calculate + '.')})        
      }else{
        this.setState({calculate: (calculate.includes('.') ? this.state.calculate + '' + value : (this.isOperand(value) ? '0' : value))})
      }
    }else{
      if(value == '.' && calculate.toString().includes('.')){
        var expressionArray = calculate.toString().match(/[^\d()]+|[\d.]+/g)
        if(!this.isOperand(expressionArray[expressionArray.length-1]) && !expressionArray[expressionArray.length-1].includes('.')){
          this.setState({calculate: this.state.calculate + '' + value})
        }
      }else if( this.isOperand(value) && this.isOperand(calculate.toString().substr(calculate.toString().length - 1))){
        this.setState({calculate: calculate.toString().slice(0, -1) + value})
      }else{
        this.setState({calculate: this.state.calculate + '' +value})
      }
    }
  }

  delNumber = () => {
    var calculate = this.state.calculate
    calculate = calculate.length == 1 ? '0' : calculate.slice(0, -1)
    this.setState({calculate: calculate})
  }

  countProcess = () => {
    try{
      let result = eval(this.state.calculate)
      this.setState({
        result: (result > 999999999 ? '' : terbilang(result) ),
        resultNumber: result == Math.floor(result) ? result+'' : parseFloat(result.toFixed(5)) + ''
      })
    }catch(e){
      this.setState({
        result: 'Invalid Operator',
        resultNumber: ''
      })
    }
  }

  render() {
    return (
      <View style={{flex: 1, backgroundColor: '#212121'}}>
        <StatusBar backgroundColor="#212121" barStyle="light-content"/>

        <View style={{flex: 0.5, justifyContent: 'center', marginHorizontal: 10}}>
          <Text style={styles.expressionText}>{this.state.calculate}</Text>
        </View>
        <View style={{flex: 0.4, justifyContent: 'center', marginHorizontal: 10}}>
          <Text style={styles.expressionText}>{this.state.resultNumber}</Text>
          <Text style={styles.resultText}>{this.state.result}</Text>
        </View>

        <View style={{flex: 1, justifyContent: 'flex-end', marginVertical: 15}}>
          <View style={{flexDirection: 'row'}}>
            <TouchableOpacity style={styles.touchableButton}
              onPress={() => this.inputNumber('(')}
            >
              <Text style={styles.buttonTextOperand}>(</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.touchableButton}
              onPress={() => this.inputNumber(')')}
            >
              <Text style={styles.buttonTextOperand}>)</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.touchableButton}
              onPress={() => this.inputNumber('/')}
            >
              <Text style={styles.buttonTextOperand}>/</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.touchableButton}
              onPress={() => this.inputNumber('*')}
            >
              <Text style={styles.buttonTextOperand}>x</Text>
            </TouchableOpacity>
          </View>

          <View style={{flexDirection: 'row'}}>
            <TouchableOpacity style={styles.touchableButton}
              onPress={() => this.inputNumber(7)}
            >
              <Text style={styles.buttonTextNumber}>7</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.touchableButton}
              onPress={() => this.inputNumber(8)}
            >
              <Text style={styles.buttonTextNumber}>8</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.touchableButton}
              onPress={() => this.inputNumber(9)}
            >
              <Text style={styles.buttonTextNumber}>9</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.touchableButton}
              onPress={() => this.inputNumber('+')}
            >
              <Text style={styles.buttonTextOperand}>+</Text>
            </TouchableOpacity>
          </View>

          <View style={{flexDirection: 'row'}}>
            <TouchableOpacity style={styles.touchableButton}
              onPress={() => this.inputNumber(4)}  
            >
              <Text style={styles.buttonTextNumber}>4</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.touchableButton}
              onPress={() => this.inputNumber(5)}
            >
              <Text style={styles.buttonTextNumber}>5</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.touchableButton}
              onPress={() => this.inputNumber(6)}
            >
              <Text style={styles.buttonTextNumber}>6</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.touchableButton}
              onPress={() => this.inputNumber('-')}
            >
              <Text style={styles.buttonTextOperand}>-</Text>
            </TouchableOpacity>
          </View>

          <View style={{flexDirection: 'row'}}>
            <TouchableOpacity style={styles.touchableButton}
              onPress={() => this.inputNumber(1)}
            >
              <Text style={styles.buttonTextNumber}>1</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.touchableButton}
              onPress={() => this.inputNumber(2)}
            >
              <Text style={styles.buttonTextNumber}>2</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.touchableButton}
              onPress={() => this.inputNumber(3)}
            >
              <Text style={styles.buttonTextNumber}>3</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.touchableButton}
              onPress={() => this.setState({calculate: '0', result: '', resultNumber: ''})}
            >
              <Text style={styles.buttonTextOperand}>clear</Text>
            </TouchableOpacity>
          </View>
          <View style={{flexDirection: 'row'}}>
            <TouchableOpacity style={styles.touchableButton}
              onPress={() => this.inputNumber('.')}
            >
              <Text style={styles.buttonTextNumber}>.</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.touchableButton}
              onPress={() => this.inputNumber('0')}
            >
              <Text style={styles.buttonTextNumber}>0</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.touchableButton}
              onPress={() => this.delNumber()}
            >
              <Text style={styles.buttonTextNumber}>del</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.touchableButton}
              onPress={() => this.countProcess()}
            >
              <Text style={styles.buttonTextOperand}>=</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

export default App;